/**
 * Generates a random number within [0,1) biased towards values close to 0.5
 */
function softRand() {
    return 4 * Math.pow(Math.random() - 0.5, 3) + 0.5;
}

/**
 * A black box algorithm for determining the Wilson Complexity Score (0.0 to 1.0) of given word
 */
function determineComplexity(word) {
    var i,a={a:1,b:3,c:3,d:2,e:1,f:4,g:2,h:4,i:1,j:8,k:5,l:1,m:3,n:1,o:1,p:3,q:10,r:1,s:1,t:1,u:1,v:4,w:4,x:8,y:4,z:10},sum=0;for(i in word)sum+=a[word[i]];
    return 2 / (1 + Math.pow(Math.E, -0.15 * sum)) - 1;
}

/**
 * A black box algorithm for determining the Wilson Meaningfulness Score of a given word
 */
function determineMeaningfulness(word) {
    return 2 / (1 + Math.pow(Math.E, -0.25 * word.length)) - 1;
}

function generatePrice(word, market) {
    return 100 * softRand() * market * (determineComplexity(word) + determineMeaningfulness(word)) / 2;
}

$.getJSON(
  "https://random-word-api.herokuapp.com/word?number=100&swear=0",
  function(words) {
    market = softRand();
    console.log("Market bias: " + market);
    for(word in words) {
      $("#words").append("<tr><td>" + words[word] + "</td><td>$" + generatePrice(words[word], market).toFixed(2) + "</td></tr>");
    }
    $("#end").html("You've reached the end.");
  }
);